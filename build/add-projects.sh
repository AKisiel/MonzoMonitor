#!/bin/bash
shopt -s globstar
for i in ../**/*.csproj; do
    dotnet sln ./MonzoMonitor.sln add $i
done