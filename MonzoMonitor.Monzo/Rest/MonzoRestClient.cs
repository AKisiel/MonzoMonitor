using System;
using MonzoMonitor.Core.Rest;
using RestSharp;

namespace MonzoMonitor.Monzo.Rest
{
    public class MonzoRestClient : IMonzoClient
    {
        protected RestSharp.RestClient Client { get; set; }
        public string BaseUrl { get; protected set; }
        
        protected MonzoRestClient(string baseUrl)
        {
            this.BaseUrl = baseUrl;
        }
        
        public MonzoRestClient() : this("https://api.monzo.com/")
        {
            
        }
    }
}